FROM php:7.2.16-fpm-alpine as basic
ENV FETCH_PACKAGES \
        libpq \
        libzip \
        git \
        openssl \
        unzip \
        zlib-dev \
        bash

ENV BUILD_PACKAGES \
        alpine-sdk \
        autoconf \
        postgresql-dev

ENV PERMANENT_PACKAGES \
        openssh \
        git \
        shadow \
        bash

ENV PHP_EXTENSIONS \
        pdo \
        pdo_mysql \
        mysqli \
        zip \
        bcmath \
        mbstring \
        sockets

ENV COMPOSE_HTTP_TIMEOUT=3600
ENV COMPOSE_PROJECT_NAME=example-service
ENV COMPOSER_ALLOW_SUPERUSER=1

RUN set -ex \
    && apk update \
    && apk add --no-cache --virtual .fetch-deps $FETCH_PACKAGES \
    && apk add --no-cache --virtual .build-deps $BUILD_PACKAGES \
    && apk add --no-cache $PERMANENT_PACKAGES \
    && docker-php-ext-install $PHP_EXTENSIONS \
    && ln -snf /usr/share/zoneinfo/Europe/Madrid /etc/localtime \
    && echo Europe/Madrid > /etc/timezone \
    && printf '[PHP]\ndate.timezone = "%s"\n', Europe/Madrid > /usr/local/etc/php/conf.d/tzone.ini \
    && apk del .fetch-deps

WORKDIR /var/www/example-service

FROM basic as builder
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

FROM builder as development
RUN pecl channel-update pecl.php.net \
	&& pecl install xdebug-2.7.2 \
	&& curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

COPY docker/php-fpm/docker-php-ext-xdebug.ini  /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

FROM development as test
ENV APP_ENV=prod
COPY ./composer.json ./composer.lock ./
RUN set -eux; \
    composer install --prefer-dist --no-scripts --no-progress --no-suggest; \
    composer clear-cache
COPY ./ /var/www/example-service

ENV XDEBUG_IDE_KEY=PHPSTORM
ENV XDEBUG_REMOTE_ENABLE=1
ENV XDEBUG_REMOTE_PORT=9000

FROM builder as result
ENV APP_ENV=prod
COPY ./ /var/www/example-service
RUN composer install --no-dev --optimize-autoloader \
    && rm -rf \
        .git \
        .gitignore \
        docker \
        documentation \
        docker-compose.yml \
        phpcs.xml.dist \
        phpunit.xml.dist \
        grumphp.yml \
        Makefile \
        phpstan.neon \
        tests

FROM basic as production
COPY --from=result /var/www/example-service ./
RUN chown -R www-data:www-data /var/www/example-service
