.DEFAULT_GOAL := help
.SILENT:
.PHONY: vendor

## Colors
COLOR_RESET   = \033[0m
COLOR_INFO    = \033[32m
COLOR_COMMENT = \033[33m

## Help
help:
	printf "${COLOR_COMMENT}Usage:${COLOR_RESET}\n"
	printf " make [target]\n\n"
	printf "${COLOR_COMMENT}Available targets:${COLOR_RESET}\n"
	awk '/^[a-zA-Z\-\_0-9\.@]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf " ${COLOR_INFO}%-16s${COLOR_RESET} %s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST)

##################
# Useful targets #
##################

## Install all install_* requirements and launch project.
install: env_file env_run install_vendor install_db import_dev install_amqp

## Run project, install vendors and run migrations.
run: env_run install_vendor install_db

## Stop project.
stop:
	docker-compose stop

## Down project and remove volumes (databases).
down:
	docker-compose down -v --remove-orphans

## Run all quality assurance tools (tests and code inspection).
qa: unsued_libraries code_static_analysis code_fixer code_detect code_correct test_spec test test_behaviour

## Truncate database and import fixtures.
fixtures: down run import_dev

########
# Code #
########

shell:
	docker-compose exec example-php sh

## Run codesniffer to correct violations of a defined coding project standards.
code_correct:
	docker-compose exec example-php bin/phpcs --standard=PSR2 src

## Run codesniffer to detect violations of a defined coding project standards.
code_detect:
	docker-compose exec example-php bin/phpcbf --standard=PSR2 src tests

## Run cs-fixer to fix php code to follow project standards.
code_fixer:
	docker-compose exec example-php bin/php-cs-fixer fix

## Run PHPStan to find errors in code.
code_static_analysis:
	docker-compose exec example-php bin/phpstan analyse src --level max

## Checking the project for the commit
code_verify:
	docker-compose exec example-php vendor/bin/grumphp run

## Check unsued libraries via composer
unsued_libraries:
	docker-compose exec example-php bin/unused_scanner scanner_config.php

###############
# Environment #
###############

## Set defaut environment variables by copying env.dist file as .env.
env_file:
	cp .env.dist .env

## Launch docker environment.
env_run:
	docker-compose up -d

###############
# Import Data #
###############

## Import fixtures.
import_dev:
	docker-compose exec example-php bin/console doctrine:fixtures:load -n

## Import data from .cat file
import_states:
	docker-compose exec example-php bin/console somecommmand:import:provinces

## Import data from .cat provider.
import_stations:
	docker-compose exec example-php bin/console somecommand:import:countries

###########
# Install #
###########

## Run database migration.
install_db:
	docker-compose run --rm example-php bin/console doctrine:migrations:migrate -n

## Run test database migration.
install_db_test:
	docker-compose exec example-php bin/console doctrine:migrations:migrate -n --env=test

## Install vendors.
install_vendor:
	docker-compose run --rm example-php composer install --prefer-dist --no-progress --no-suggest

update_vendor:
	docker-compose run --rm example-php composer update --prefer-dist --no-progress --no-suggest

## Install amqp.
install_amqp:
	docker-compose run --rm example-php bin/console rabbitmq:setup-fabric

########
# Test#
########

## Run unit&integration tests with pre-installing test database.
test: install_db_test test_unit

## Run mutant testing
test_mutant:
	docker-compose exec example-php vendor/bin/infection

## Run behaviour tests.
test_behaviour:
	docker-compose exec example-php vendor/bin/behat

## Run unit&integration tests.
test_unit:
	docker-compose exec example-php vendor/bin/phpunit

## Run unit&integration tests with coverage
test_unit_coverage:
	docker-compose exec example-php vendor/bin/phpunit --coverage-text

## Run php spect tests.
test_spec:
	docker-compose exec example-php bin/phpspec run

#########
# Utils #
#########

db_diff:
	docker-compose run --rm example-php bin/console doctrine:migrations:diff -n

db_update: db_diff install_db

#########
# AMQP  #
#########

## Process one event
amqp_event:
	docker-compose run --rm example-php bin/console rabbitmq:consumer -m 1 consumer.domain_event

## Process one command
amqp_command:
	docker-compose run --rm example-php bin/console rabbitmq:consumer -m 1 consumer.command

## Process all once
amqp_all: amqp_event amqp_command

#########
# AMQP  #
#########
docker_enter:
	docker-compose run --rm example-php sh