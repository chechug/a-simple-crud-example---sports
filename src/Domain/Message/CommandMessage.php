<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message;

abstract class CommandMessage extends SimpleMessage
{
    final public static function messageType(): string
    {
        return 'command';
    }
}
