<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message\Types;

use ChechuProjects\Sports\Domain\Message\SimpleMessage;

abstract class QueryEvent extends SimpleMessage
{
    final public static function messageType(): string
    {
        return 'query_event';
    }
}
