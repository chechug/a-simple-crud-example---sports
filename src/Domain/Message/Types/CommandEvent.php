<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message\Types;

use ChechuProjects\Sports\Domain\Message\SimpleMessage;

abstract class CommandEvent extends SimpleMessage
{
    final public static function messageType(): string
    {
        return 'command_event';
    }
}
