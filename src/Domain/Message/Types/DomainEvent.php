<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message\Types;

use ChechuProjects\Sports\Domain\Message\AggregateMessage;

abstract class DomainEvent extends AggregateMessage
{
    final public static function messageType(): string
    {
        return 'domain_event';
    }
}
