<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message;

use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;

abstract class SimpleMessage extends Message
{
    final public static function fromPayload(Uuid $messageId, array $payload): self
    {
        $message = new static($messageId, $payload);
        $message->assertPayload();

        return $message;
    }

    abstract protected function assertPayload(): void;
}
