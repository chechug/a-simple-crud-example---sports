<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message;

use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;

abstract class Message implements \JsonSerializable
{
    private $messageId;
    private $payload;

    protected function __construct(Uuid $messageId, array $payload)
    {
        $this->assertPrimitivePayload($payload);
        $this->messageId = $messageId;
        $this->payload = $payload;
    }

    public function messageId(): Uuid
    {
        return $this->messageId;
    }

    public function messagePayload(): array
    {
        return $this->payload;
    }

    public function jsonSerialize(): array
    {
        return [
            'message_id' => $this->messageId(),
            'name' => static::messageName(),
            'version' => static::messageVersion(),
            'type' => static::messageType(),
            'payload' => $this->messagePayload(),
        ];
    }

    private function assertPrimitivePayload(array &$payload, string $index = 'payload'): void
    {
        \array_walk(
            $payload,
            function ($item, $currentIndex) use ($index) {
                $newIndex = "{$index}.{$currentIndex}";
                if (\is_object($item)) {
                    throw new \InvalidArgumentException(
                        sprintf(
                            'Attribute %s is an object. Payload parameters only can be primitive.',
                            $newIndex
                        )
                    );
                } elseif (\is_array($item)) {
                    $this->assertPrimitivePayload($item, $newIndex);
                }
            }
        );
    }

    abstract public static function messageName(): string;
    abstract public static function messageVersion(): int;
    abstract public static function messageType(): string;
}
