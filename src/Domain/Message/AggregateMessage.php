<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Message;

use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;

abstract class AggregateMessage extends Message
{
    private $aggregateId;
    private $occurredOn;
    private $aggregateVersion;

    final protected function __construct(
        Uuid $messageId,
        Uuid $aggregateId,
        int $aggregateVersion,
        DateTimeValueObject $occurredOn,
        array $payload
    ) {
        parent::__construct($messageId, $payload);
        $this->aggregateId = $aggregateId;
        $this->aggregateVersion = $aggregateVersion;
        $this->occurredOn = $occurredOn;
    }

    final public function aggregateId(): Uuid
    {
        return $this->aggregateId;
    }

    final public function occurredOn(): \DateTimeInterface
    {
        return $this->occurredOn;
    }

    final public function jsonSerialize(): array
    {
        return \array_merge(
            parent::jsonSerialize(),
            [
                'aggregate_id' => $this->aggregateId,
                'aggregate_version' => $this->aggregateVersion,
                'occurred_on' => $this->occurredOn,
            ]
        );
    }

    final public static function fromPayload(
        Uuid $messageId,
        Uuid $aggregateId,
        DateTimeValueObject $occurredOn,
        array $payload,
        int $aggregateVersion = 0
    ): self {
        $message = new static($messageId, $aggregateId, $aggregateVersion, $occurredOn, $payload);
        $message->hydrateEventByPayload();

        return $message;
    }

    final public function aggregateVersion(): int
    {
        return $this->aggregateVersion;
    }

    abstract protected function hydrateEventByPayload(): void;
}
