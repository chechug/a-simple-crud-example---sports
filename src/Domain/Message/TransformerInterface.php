<?php

namespace ChechuProjects\Sports\Domain\Message;

interface TransformerInterface
{
    public function execute(AggregateMessage $event): SimpleMessage;
}
