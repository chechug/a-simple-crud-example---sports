<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain;

use ChechuProjects\Sports\Domain\Message\Message;

interface DomainEventSubscriber
{
    public function handle(Message $aDomainEvent);
    public function isSubscribedTo(Message $aDomainEvent);
}
