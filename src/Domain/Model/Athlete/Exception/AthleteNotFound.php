<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\Exception;

use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use Pccomponentes\Apixception\Core\Exception\NotFoundException;

final class AthleteNotFound extends NotFoundException
{
    private $type;
    private $value;
    private const EXCEPTION_CODE = '1004';

    public static function fromId(Uuid $id): self
    {
        return new self('id', $id->value());
    }

    private function __construct(string $type, string $value)
    {
        $this->type = $type;
        $this->value = $value;
        parent::__construct('Athlete not found');
    }

    public function value(): string
    {
        return $this->value;
    }

    public function id(): string
    {
        return self::EXCEPTION_CODE;
    }

    public function resource(): string
    {
        return $this->type;
    }
}
