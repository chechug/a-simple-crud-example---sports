<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\Exception;

use Pccomponentes\Apixception\Core\Exception\LogicException;

// TODO - Sacar esta excepción de aquí, es de infraestructura!
final class InvalidJSONException extends LogicException
{
    public function __construct()
    {
        parent::__construct('JSON is not valid');
    }

    public function data(): array
    {
        return [
            'value' => '',
            'assert' => [
            ],
        ];
    }
}
