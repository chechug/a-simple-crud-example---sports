<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\Event\VersionOne\AthleteCreated;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use Exception;
use ChechuProjects\Sports\Domain\DomainEventPublisher;
use ChechuProjects\Sports\Domain\Model\AggregateRoot;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;

class Athlete implements AggregateRoot
{
    private $id;
    private $name;
    private $sport;
    private $birthday;
    private $country;

    /**
     * @throws Exception
     */
    public static function create(
        Uuid $id,
        AthleteName $name,
        AthleteSport $sport,
        DateTimeValueObject $birthday,
        AthleteCountryCode $countryCode
    ): self {
        $athlete = new self($id, $name, $sport, $birthday, $countryCode);

        // TODO - Lanzar este evento en servicio de Crear deportista cuando se haga. (Personal)
        DomainEventPublisher::instance()->publish(
            AthleteCreated::from(
                $athlete->id(),
                $athlete->name(),
                $athlete->sport(),
                $athlete->birthday(),
                $athlete->country()
            )
        );

        return $athlete;
    }

    public function id(): Uuid
    {
        return $this->id;
    }

    public function name(): AthleteName
    {
        return $this->name;
    }

    public function sport(): AthleteSport
    {
        return $this->sport;
    }

    public function birthday(): DateTimeValueObject
    {
        return $this->birthday;
    }

    public function country(): AthleteCountryCode
    {
        return $this->country;
    }

    public static function modelName(): string
    {
        return 'athlete';
    }

    private function __construct(
        Uuid $id,
        AthleteName $name,
        AthleteSport $sport,
        DateTimeValueObject $birthday,
        AthleteCountryCode $countryCode
    ) {
        $this->id = $id;
        $this->name = $name;
        $this->sport = $sport;
        $this->birthday = $birthday;
        $this->country = $countryCode;
    }
}
