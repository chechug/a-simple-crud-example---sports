<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\ValueObject;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\Exception\AthleteCountryCodeException;
use ChechuProjects\Sports\Domain\Model\ValueObject\StringValueObject;
use Symfony\Component\Intl\Intl;

final class AthleteCountryCode extends StringValueObject
{
    protected function __construct(string $name)
    {
        $name = strtoupper($name);

        $countryName = Intl::getRegionBundle()->getCountryName($name);

        if (null === $countryName) {
            throw new AthleteCountryCodeException($name);
        }

        parent::__construct($name);
    }
}
