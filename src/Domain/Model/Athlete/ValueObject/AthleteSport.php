<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\ValueObject;

use ChechuProjects\Sports\Domain\Model\ValueObject\EnumValueObject;

final class AthleteSport extends EnumValueObject
{
    public const FOOTBALL = 'FOOTBALL';
    public const BASKET = 'BASKET';
    public const HOCKEY = 'HOCKEY';
    public const OTHER = 'OTHER';
}
