<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\Exception;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use Pccomponentes\Apixception\Core\Exception\LogicException;

final class AthleteNameException extends LogicException
{
    private $value;

    public function __construct(string $value)
    {
        parent::__construct(
            $value.
            ' is not a valid player name'
        );

        $this->value = $value;
    }

    public function data(): array
    {
        return [
            'value' => $this->value,
            'assert' => [
                'max_length' => AthleteName::MAX_LENGTH,
                'min_length' => AthleteName::MIN_LENGTH,
            ],
        ];
    }
}
