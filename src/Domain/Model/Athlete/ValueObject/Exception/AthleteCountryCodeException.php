<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\Exception;

use Pccomponentes\Apixception\Core\Exception\LogicException;
use Symfony\Component\Intl\Intl;

final class AthleteCountryCodeException extends LogicException
{
    private $value;

    public function __construct(string $value)
    {
        parent::__construct(
            $value.
            ' is not a valid country code'
        );

        $this->value = $value;
    }

    public function data(): array
    {
        return [
            'value' => $this->value,
            'assert' => [
                'valid' => Intl::getRegionBundle()->getCountryNames(),
            ],
        ];
    }
}
