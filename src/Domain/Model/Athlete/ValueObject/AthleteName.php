<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\ValueObject;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\Exception\AthleteNameException;
use ChechuProjects\Sports\Domain\Model\ValueObject\StringValueObject;

final class AthleteName extends StringValueObject
{
    public const MAX_LENGTH = 255;
    public const MIN_LENGTH = 1;

    /**
     * @throws AthleteNameException
     */
    protected function __construct(string $name)
    {
        switch (true) {
            case strlen($name) < self::MIN_LENGTH:
            case strlen($name) > self::MAX_LENGTH:
                throw new AthleteNameException($name);
        }

        parent::__construct($name);
    }
}
