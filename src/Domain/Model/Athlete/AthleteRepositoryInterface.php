<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;

interface AthleteRepositoryInterface
{
    public function searchOne(Uuid $id): ?Athlete;
    public function addAndSave(Athlete $athlete): void;

    /**
     * @return Athlete []
     */
    public function filter(
        ?Uuid $id,
        ?AthleteName $name,
        ?AthleteSport $sport,
        ?AthleteCountryCode $country
    ): array;

    public function save(): void;
    public function removeAndSave(Athlete $athlete): void;
    public function beginTransaction(): void;
    public function commit(): void;
}
