<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\Athlete\Event\VersionOne;

use ChechuProjects\Sports\Domain\Message\AggregateMessage;
use ChechuProjects\Sports\Domain\Message\Types\DomainEvent;
use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use Exception;

class AthleteCreated extends DomainEvent
{
    private const NAME = 'athlete_created';
    private const VERSION = 1;

    private $id;
    private $name;
    private $sport;
    private $birthday;
    private $countryCode;

    /**
     * @throws Exception
     */
    public static function from(
        Uuid $id,
        AthleteName $name,
        AthleteSport $sport,
        DateTimeValueObject $birthday,
        AthleteCountryCode $countryCode
    ): AggregateMessage {
        return self::fromPayload(
            Uuid::v4(),
            $id,
            new DateTimeValueObject(),
            [
                'id' => $id->value(),
                'name' => $name->value(),
                'sport' => $sport->value(),
                'birthday' => $birthday->format('Y-m-d'),
                'countryCode' => $countryCode->value(),
            ]
        );
    }

    protected function hydrateEventByPayload(): void
    {
        $payload = $this->messagePayload();
        $this->id = Uuid::from($payload['id']);
        $this->name = AthleteName::from($payload['name']);
        $this->sport = AthleteSport::from($payload['sport']);
        $this->birthday = DateTimeValueObject::from($payload['birthday']);
        $this->countryCode = AthleteCountryCode::from($payload['countryCode']);
    }

    public static function messageName(): string
    {
        return 'chechuprojects.'
            .'sports.'
            .self::VERSION.'.'
            .self::messageType().'.'
            .Athlete::modelName().'.'
            .self::NAME;
    }

    public static function messageVersion(): int
    {
        return self::VERSION;
    }

    public function id()
    {
        return $this->id;
    }

    public function name()
    {
        return $this->name;
    }

    public function sport()
    {
        return $this->sport;
    }

    public function birthday()
    {
        return $this->birthday;
    }

    public function countryCode()
    {
        return $this->countryCode;
    }
}
