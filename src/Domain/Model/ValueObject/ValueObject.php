<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\ValueObject;

use JsonSerializable;

interface ValueObject extends JsonSerializable
{
}
