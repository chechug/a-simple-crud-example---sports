<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\ValueObject;

use const DATE_ATOM;
use DateTimeImmutable;
use DateTimeZone;
use InvalidArgumentException;

class DateTimeValueObject extends DateTimeImmutable implements ValueObject
{
    final public function jsonSerialize(): string
    {
        return $this->format(DATE_ATOM);
    }

    final public static function from(string $str): self
    {
        return new static($str, new DateTimeZone('UTC'));
    }

    final public static function fromTimestamp(int $timestamp): self
    {
        $dateTime = DateTimeImmutable::createFromFormat('U', (string) $timestamp);

        if (false === $dateTime) {
            throw new InvalidArgumentException('Timestamp not valid');
        }

        return static::from($dateTime->format(DATE_ATOM));
    }
}
