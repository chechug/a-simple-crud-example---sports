<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model\ValueObject;

use function array_filter;
use function array_key_exists;
use function array_map;
use function array_reduce;
use function array_values;
use function array_walk;
use function count;
use Countable;
use function current;
use function get_class;
use Iterator;
use function key;
use function next;
use function reset;

class CollectionValueObject implements Iterator, Countable, ValueObject
{
    private $items;

    protected function __construct(array $items)
    {
        $this->items = $items;
    }

    public function current()
    {
        return current($this->items);
    }

    public function next()
    {
        next($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function valid()
    {
        return array_key_exists($this->key(), $this->items);
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function count()
    {
        return count($this->items);
    }

    public function walk(callable $func)
    {
        array_walk($this->items, $func);
    }

    public function filter(callable $func)
    {
        return new static(array_values(array_filter($this->items, $func)));
    }

    public function map(callable $func): CollectionValueObject
    {
        return new self(array_map($func, $this->items));
    }

    public function reduce(callable $func, $initial)
    {
        return array_reduce($this->items, $func, $initial);
    }

    public function isEmpty(): bool
    {
        return 0 === $this->count();
    }

    public function equalTo(CollectionValueObject $other): bool
    {
        return get_class($other) === static::class && $this->items == $other->items;
    }

    final public function jsonSerialize(): array
    {
        return $this->items;
    }

    protected function addItem($item): self
    {
        $items = $this->items;
        $items[] = $item;

        return new static($items);
    }

    protected function removeItem($item): self
    {
        return $this->filter(
            static function ($current) use ($item) {
                return $current !== $item;
            }
        );
    }

    public static function from(array $items)
    {
        return new static($items);
    }
}
