<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model;

interface AggregateRoot
{
    public static function modelName(): string;
}
