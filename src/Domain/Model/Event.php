<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Domain\Model;

class Event
{
    private $id;
    private $eventName;
    private $eventBody;
    private $occurredOn;

    public function __construct(string $eventName, string $eventBody, \DateTimeInterface $occurredOn)
    {
        $this->eventName = $eventName;
        $this->eventBody = $eventBody;
        $this->occurredOn = $occurredOn;
    }

    public function id(): int
    {
        return $this->id;
    }

    public function eventName(): string
    {
        return $this->eventName;
    }

    public function occurredOn(): \DateTimeInterface
    {
        return $this->occurredOn;
    }

    public function eventBody(): string
    {
        return $this->eventBody;
    }
}
