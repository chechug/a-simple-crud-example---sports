<?php
namespace ChechuProjects\Sports\Infrastructure\Messaging;

final class SimpleMessageStream
{
    private $messageId;
    private $messageName;
    private $payload;

    public function __construct(string $messageId, string $messageName, string $payload)
    {
        $this->messageId = $messageId;
        $this->messageName = $messageName;
        $this->payload = $payload;
    }

    public function messageId(): string
    {
        return $this->messageId;
    }

    public function messageName(): string
    {
        return $this->messageName;
    }

    public function payload(): string
    {
        return $this->payload;
    }
}
