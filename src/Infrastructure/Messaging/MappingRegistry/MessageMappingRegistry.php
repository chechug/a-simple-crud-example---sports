<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\MappingRegistry;

final class MessageMappingRegistry
{
    private $registry;

    public function __construct(?array $registry)
    {
        $this->registry = $registry;
    }

    public function execute(string $messageName): ?string
    {
        foreach ($this->registry as $item) {
            if ($item && array_key_exists($messageName, $item)) {
                return $item[$messageName];
            }
        }

        return null;
    }
}
