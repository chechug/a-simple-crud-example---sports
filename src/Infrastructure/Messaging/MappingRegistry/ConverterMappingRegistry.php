<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\MappingRegistry;

final class ConverterMappingRegistry
{
    private $registry;

    public function __construct(?array $registry)
    {
        $this->registry = $registry;
    }

    public function execute(string $messageName): ?array
    {
        $slugEventName = $this->getSlugNameEvent($messageName);
        $namespace = $this->getNamespaceEvent($messageName);

        return isset($this->registry[0][$namespace][$slugEventName])
            ? $this->registry[0][$namespace][$slugEventName]
            : null;
    }

    private function getSlugNameEvent(string $messageName): ?string
    {
        preg_match_all(
            '/\w+\z/',
            $messageName,
            $slugEventName,
            PREG_SET_ORDER,
            0
        );

        return isset($slugEventName[0][0]) ? $slugEventName[0][0] : null;
    }

    private function getNamespaceEvent(string $messageName): ?string
    {
        preg_match(
            '/^[a-z0-9-_]+\.+[a-z0-9-_]+\.+[a-z0-9-_]+\.+[a-z0-9-_]+\.+[a-z0-9-_]+/',
            $messageName,
            $namespace,
            PREG_OFFSET_CAPTURE,
            0
        );

        return isset($this->registry[0][$namespace[0][0]]) ? $namespace[0][0] : null;
    }
}
