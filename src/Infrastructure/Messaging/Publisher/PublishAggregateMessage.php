<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Publisher;

use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use ChechuProjects\Sports\Domain\Message\AggregateMessage;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\AggregateMessage\JsonSerializer;

class PublishAggregateMessage
{
    protected $producer;
    protected $messageSerializer;

    public function __construct(ProducerInterface $producer, JsonSerializer $messageSerializer)
    {
        $this->producer = $producer;
        $this->messageSerializer = $messageSerializer;
    }

    public function publish(AggregateMessage $message)
    {
        $this->producer->publish(
            $this->messageSerializer->serialize($message),
            $this->messageSerializer->routingKey($message)
        );
    }
}
