<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber;

use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Exception\ClassNotFoundException;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\Exception\MessageClassNotFoundException;

interface SubscriberCallbackInterface
{
    /**
     * @throws ClassNotFoundException
     * @throws MessageClassNotFoundException
     */
    public function execute(SubscriberMessage $message);
}
