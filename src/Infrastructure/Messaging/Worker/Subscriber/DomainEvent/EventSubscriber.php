<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\DomainEvent;

use Assert\Assert;
use Monolog\Logger;
use ChechuProjects\Sports\Domain\Message\AggregateMessage;
use ChechuProjects\Sports\Domain\Message\TransformerInterface;
use ChechuProjects\Sports\Infrastructure\Messaging\MessageStream;
use ChechuProjects\Sports\Infrastructure\Messaging\Publisher\PublishSimpleMessage;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\SubscriberMessage;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Exception\ClassNotFoundException;
use ChechuProjects\Sports\Infrastructure\Messaging\MappingRegistry\ConverterMappingRegistry;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\AggregateMessage\JsonDeserializer;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\SubscriberCallbackInterface;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\Exception\MessageClassNotFoundException;

class EventSubscriber implements SubscriberCallbackInterface
{
    private $deserializer;
    private $publisher;
    private $converterMappingRegistry;
    private $logger;

    public function __construct(
        JsonDeserializer $deserializer,
        PublishSimpleMessage $publisher,
        ConverterMappingRegistry $converterMappingRegistry,
        Logger $logger
    ) {
        $this->deserializer = $deserializer;
        $this->publisher = $publisher;
        $this->converterMappingRegistry = $converterMappingRegistry;
        $this->logger = $logger;
    }

    /**
     * @throws ClassNotFoundException
     * @throws MessageClassNotFoundException
     */
    public function execute(SubscriberMessage $message)
    {
        $messageStream = $this->createMessageStream($message);
        $event = $this->deserializer->deserializer($messageStream);
        $namespaceConverters = $this->converterMappingRegistry->execute($event->messageName());

        if (null === $namespaceConverters) {
            return;
        }

        $this->publishCommand(
            $this->generateCommand($event, $namespaceConverters)
        );
    }

    private function assertContent(array $content)
    {
        Assert::lazy()->tryAll()
            ->that($content['data'], 'data')->isArray()
            ->keyExists('message_id')
            ->keyExists('type')
            ->keyExists('occurred_on')
            ->keyExists('attributes')
            ->verifyNow()
        ;
        Assert::lazy()->tryAll()
            ->that($content['data']['message_id'], 'message_id')->uuid()
            ->that($content['data']['type'], 'type')->string()->notEmpty()
            ->that($content['data']['occurred_on'], 'occurred_on')->notEmpty()
            ->that($content['data']['attributes'], 'attributes')->isArray()->keyExists('aggregate_id')
            ->verifyNow();
        Assert::lazy()->tryAll()
            ->that($content['data']['attributes']['aggregate_id'], 'aggregate_id')->uuid()
            ->verifyNow();
    }

    /**
     * @throws ClassNotFoundException
     */
    private function generateCommand(AggregateMessage $event, array $namespaceConverters): array
    {
        $command = [];
        foreach ($namespaceConverters as $namespaceConverter) {
            if (false === class_exists($namespaceConverter)) {
                throw new ClassNotFoundException('Message '.$namespaceConverter.' not found');
            }

            $command[] = $this->createConverter($namespaceConverter)
                ->execute($event)
            ;
        }

        return $command;
    }

    private function createConverter($namespaceConverter): TransformerInterface
    {
        return new $namespaceConverter();
    }

    private function createMessageStream(SubscriberMessage $message): MessageStream
    {
        $body = json_decode($message->message()->getBody(), true);

        if (null === $body) {
            throw new \InvalidArgumentException('The body of message is null');
        }

        $this->assertContent($body);
        $payload = json_encode($body['data']['attributes']);

        return new MessageStream(
            $body['data']['message_id'],
            $body['data']['attributes']['aggregate_id'],
            $body['data']['occurred_on'],
            $body['data']['type'],
            0,
            $payload ?: ''
        );
    }

    private function publishCommand(array $commands): void
    {
        foreach ($commands as $command) {
            $this->publisher->publish($command);
        }
    }
}
