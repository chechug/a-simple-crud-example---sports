<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\CommandEvent;

use Assert\Assert;
use Monolog\Logger;
use League\Tactician\CommandBus;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\Exception\MessageClassNotFoundException;
use ChechuProjects\Sports\Infrastructure\Messaging\SimpleMessageStream;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\SubscriberMessage;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\SimpleMessage\JsonDeserializer;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\SubscriberCallbackInterface;

class CommandSubscriber implements SubscriberCallbackInterface
{
    private $commandBus;
    private $messageDeserializable;
    private $logger;

    public function __construct(
        CommandBus $commandBus,
        JsonDeserializer $messageDeserializable,
        Logger $logger
    ) {
        $this->commandBus = $commandBus;
        $this->messageDeserializable = $messageDeserializable;
        $this->logger = $logger;
    }

    /**
     * @throws MessageClassNotFoundException
     */
    public function execute(SubscriberMessage $message)
    {
        $messageStream = $this->createMessageStream($message);
        $command = $this->messageDeserializable->deserializer($messageStream);

        $this->commandBus->handle($command);
    }


    private function createMessageStream(SubscriberMessage $message): SimpleMessageStream
    {
        $body = json_decode($message->message()->getBody(), true);
        $this->assertContent($body);

        if (null === $body) {
            throw new \InvalidArgumentException('The body of message is null');
        }


        $payload = json_encode($body['data']['attributes']);

        return new SimpleMessageStream(
            $body['data']['message_id'],
            $body['data']['type'],
            $payload ?: ''
        );
    }

    private function assertContent(?array $content)
    {
        Assert::lazy()->tryAll()
            ->that($content['data'], 'data')->isArray()
            ->keyExists('message_id')
            ->keyExists('type')
            ->keyExists('attributes')
            ->verifyNow()
        ;
        Assert::lazy()->tryAll()
            ->that($content['data']['message_id'], 'message_id')->uuid()
            ->that($content['data']['type'], 'type')->string()->notEmpty()
            ->that($content['data']['attributes'], 'attributes')->isArray()
            ->keyExists('aggregate_id')->isArray()
            ->verifyNow()
        ;
    }
}
