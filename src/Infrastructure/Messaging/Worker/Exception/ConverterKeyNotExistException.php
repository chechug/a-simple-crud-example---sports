<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Worker\Exception;

use Exception;

final class ConverterKeyNotExistException extends Exception
{
}
