<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Worker;

use Monolog\Logger;
use PhpAmqpLib\Message\AMQPMessage;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\SubscriberCallbackInterface;
use ChechuProjects\Sports\Infrastructure\Messaging\Worker\Subscriber\SubscriberMessage;

final class ConsumerMessage implements ConsumerInterface
{
    private $subscriberCallback;
    private $logger;

    public function __construct(
        SubscriberCallbackInterface $subscriberCallback,
        Logger $logger
    ) {
        $this->subscriberCallback = $subscriberCallback;
        $this->logger = $logger;
    }

    public function execute(AMQPMessage $msg)
    {
        try {
            $this->subscriberCallback->execute(new SubscriberMessage($msg));
        } catch (\Exception $exception) {
            $this->logger->error($exception->getMessage());
            $this->logger->error(json_encode($msg));

            return ConsumerInterface::MSG_REJECT;
        }

        return ConsumerInterface::MSG_ACK;
    }
}
