<?php
namespace ChechuProjects\Sports\Infrastructure\Messaging\Serializer\SimpleMessage;

use InvalidArgumentException;
use ChechuProjects\Sports\Domain\Message\SimpleMessage;

class JsonSerializer
{
    public function serialize(SimpleMessage $message): string
    {
        $returnData = json_encode(
            [
                'data' => [
                    'message_id' => $message->messageId(),
                    'type' => $message::messageName(),
                    'attributes' => $message->messagePayload(),
                ],
            ]
        );

        if (false === $returnData) {
            throw new InvalidArgumentException('Message not valid');
        }

        return $returnData;
    }

    public function routingKey(SimpleMessage $message): string
    {
        return $message::messageName();
    }
}
