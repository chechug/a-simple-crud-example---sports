<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Serializer\SimpleMessage;

use ChechuProjects\Sports\Domain\Message\SimpleMessage;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Infrastructure\Messaging\SimpleMessageStream;
use ChechuProjects\Sports\Infrastructure\Messaging\MappingRegistry\MessageMappingRegistry;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\Exception\MessageClassNotFoundException;

class JsonDeserializer
{
    private $registry;

    public function __construct(MessageMappingRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @throws MessageClassNotFoundException
     */
    public function deserializer(SimpleMessageStream $message): SimpleMessage
    {
        $commandClass = $this->registry->execute($message->messageName());

        if (null === $commandClass || false === class_exists($commandClass)) {
            throw new MessageClassNotFoundException('Message '.$commandClass.' not found');
        }

        return $commandClass::fromPayload(
            Uuid::from($message->messageId()),
            json_decode($message->payload(), true)
        );
    }
}
