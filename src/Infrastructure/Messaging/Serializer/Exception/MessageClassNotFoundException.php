<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Serializer\Exception;

use Exception;

final class MessageClassNotFoundException extends Exception
{
}
