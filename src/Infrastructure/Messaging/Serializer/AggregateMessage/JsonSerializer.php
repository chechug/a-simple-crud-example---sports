<?php

namespace ChechuProjects\Sports\Infrastructure\Messaging\Serializer\AggregateMessage;

use InvalidArgumentException;
use ChechuProjects\Sports\Domain\Message\AggregateMessage;

final class JsonSerializer
{
    public function serialize(AggregateMessage $message): string
    {
        $returnData = json_encode(
            [
                'data' => [
                    'message_id' => $message->messageId(),
                    'type' => $message::messageName(),
                    'occurred_on' => $message->occurredOn()->getTimestamp(),
                    'attributes' => array_merge(
                        ['aggregate_id' => $message->aggregateId()->value()],
                        $message->messagePayload()
                    ),
                    'aggregate_version' => $message->messageVersion(),
                ],
            ]
        );

        if (false === $returnData) {
            throw new InvalidArgumentException('Message not valid');
        }

        return $returnData;
    }

    public function routingKey($message): string
    {
        return $this->messageName($message);
    }

    private function messageName(AggregateMessage $message)
    {
        return $message::messageName();
    }
}
