<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Messaging\Serializer\AggregateMessage;

use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Domain\Message\AggregateMessage;
use ChechuProjects\Sports\Infrastructure\Messaging\MessageStream;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;
use ChechuProjects\Sports\Infrastructure\Messaging\MappingRegistry\MessageMappingRegistry;
use ChechuProjects\Sports\Infrastructure\Messaging\Serializer\Exception\MessageClassNotFoundException;

class JsonDeserializer
{
    private $registry;

    public function __construct(MessageMappingRegistry $registry)
    {
        $this->registry = $registry;
    }

    /**
     * @throws MessageClassNotFoundException
     */
    public function deserializer(MessageStream $message): AggregateMessage
    {
        $eventClass = $this->registry->execute($message->messageName());

        if (null === $eventClass || false === class_exists($eventClass)) {
            throw new MessageClassNotFoundException('Event '.$eventClass.' not found');
        }

        return $eventClass::fromPayload(
            Uuid::from($message->messageId()),
            Uuid::from($message->aggregateId()),
            DateTimeValueObject::fromTimestamp($message->occurredOn()),
            json_decode($message->payload(), true)
        );
    }
}
