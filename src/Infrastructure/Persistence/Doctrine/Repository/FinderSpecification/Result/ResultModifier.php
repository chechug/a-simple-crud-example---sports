<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Result;

use Doctrine\ORM\AbstractQuery;

interface ResultModifier
{
    public function hydrate(AbstractQuery $query): void;
}
