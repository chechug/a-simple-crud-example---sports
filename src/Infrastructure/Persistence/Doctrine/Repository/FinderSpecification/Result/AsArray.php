<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Result;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;

final class AsArray implements ResultModifier
{
    private $parent;

    public function __construct(FinderSpecification $parent)
    {
        $this->parent = $parent;
    }

    public function match(QueryBuilder $queryBuilder, string $alias)
    {
        return $this->parent->match($queryBuilder, $alias);
    }

    public function hydrate(AbstractQuery $query): void
    {
        $query->setHydrationMode(Query::HYDRATE_ARRAY);
    }
}
