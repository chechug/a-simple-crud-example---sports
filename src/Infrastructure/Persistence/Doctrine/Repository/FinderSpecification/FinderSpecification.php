<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification;

use Doctrine\ORM\QueryBuilder;

interface FinderSpecification
{
    public function match(QueryBuilder $queryBuilder, string $alias);
}
