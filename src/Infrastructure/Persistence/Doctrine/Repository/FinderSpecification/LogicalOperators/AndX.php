<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\LogicalOperators;

use Doctrine\ORM\QueryBuilder;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;

final class AndX implements FinderSpecification
{
    private $filters;

    public function __construct(FinderSpecification ...$filters)
    {
        $this->filters = $filters;
    }

    public function match(QueryBuilder $queryBuilder, string $alias)
    {
        return call_user_func_array(
            [$queryBuilder->expr(), 'andX'],
            array_map(
                static function ($specification) use ($queryBuilder, $alias) {
                    return $specification->match($queryBuilder, $alias);
                },
                $this->filters
            )
        );
    }
}
