<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;

final class FilterAthleteName implements FinderSpecification
{
    private $name;

    public function __construct(?AthleteName $name)
    {
        $this->name = $name;
    }

    public function match(QueryBuilder $queryBuilder, string $alias): ?Comparison
    {
        if (null === $this->name) {
            return null;
        }

        $queryBuilder->setParameter('name', $this->name);
        return $queryBuilder->expr()->eq("{$alias}.name", ':name');
    }
}
