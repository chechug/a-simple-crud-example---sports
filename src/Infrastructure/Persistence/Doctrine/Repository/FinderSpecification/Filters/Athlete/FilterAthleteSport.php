<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;

final class FilterAthleteSport implements FinderSpecification
{
    private $sport;

    public function __construct(?AthleteSport $sport)
    {
        $this->sport = $sport;
    }

    public function match(QueryBuilder $queryBuilder, string $alias): ?Comparison
    {
        if (null === $this->sport) {
            return null;
        }

        $queryBuilder->setParameter('sport', $this->sport);
        return $queryBuilder->expr()->eq("{$alias}.sport", ':sport');
    }
}
