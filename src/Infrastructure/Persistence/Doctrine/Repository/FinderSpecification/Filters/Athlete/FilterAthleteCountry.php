<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;

final class FilterAthleteCountry implements FinderSpecification
{
    private $country;

    public function __construct(?AthleteCountryCode $country)
    {
        $this->country = $country;
    }

    public function match(QueryBuilder $queryBuilder, string $alias): ?Comparison
    {
        if (null === $this->country) {
            return null;
        }

        $queryBuilder->setParameter('country', $this->country);
        return $queryBuilder->expr()->eq("{$alias}.country", ':country');
    }
}
