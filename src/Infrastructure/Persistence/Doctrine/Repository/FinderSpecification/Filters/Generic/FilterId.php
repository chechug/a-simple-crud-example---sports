<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Generic;

use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;

final class FilterId implements FinderSpecification
{
    private $id;

    public function __construct(?Uuid $id)
    {
        $this->id = $id;
    }

    public function match(QueryBuilder $queryBuilder, string $alias): ?Comparison
    {
        if (null === $this->id) {
            return null;
        }

        $queryBuilder->setParameter('id', $this->id);
        return $queryBuilder->expr()->eq("{$alias}.id", ':id');
    }
}
