<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository;

use Doctrine\Common\Persistence\ManagerRegistry;
use InvalidArgumentException;
use ChechuProjects\Sports\Domain\Model\Event;
use ChechuProjects\Sports\Application\EventStore;
use ChechuProjects\Sports\Domain\Message\Types\DomainEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class EventDoctrineRepository extends ServiceEntityRepository implements EventStore
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Event::class);
    }

    public function append(DomainEvent $aDomainEvent)
    {
        $encodedDomainEvent = json_encode($aDomainEvent);

        if (false === $encodedDomainEvent) {
            throw new InvalidArgumentException('Domain event on persist invalid');
        }

        $this->getEntityManager()->persist(new Event(
            $aDomainEvent->messageName(),
            $encodedDomainEvent,
            $aDomainEvent->occurredOn()
        ));
    }

    public function allStoredEventsSince($anEventId)
    {
        // TODO: Implement allStoredEventsSince() method.
    }
}
