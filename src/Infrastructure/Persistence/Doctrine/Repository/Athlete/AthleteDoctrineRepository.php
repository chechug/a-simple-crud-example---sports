<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\AthleteRepositoryInterface;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete\FilterAthleteCountry;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete\FilterAthleteName;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete\FilterAthleteSport;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Generic\FilterId;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\LogicalOperators\AndX;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Result\AsArray;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;

final class AthleteDoctrineRepository extends ServiceEntityRepository implements AthleteRepositoryInterface
{
    public function __construct(ManagerRegistry $managerRegistry)
    {
        parent::__construct($managerRegistry, Athlete::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function searchOne(Uuid $id): ?Athlete
    {
        return $this
            ->getEntityManager()
            ->createQueryBuilder()
            ->select('a')
            ->from(Athlete::class, 'a')
            ->where('a.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @return Athlete []
     */
    public function filter(
        ?Uuid $id,
        ?AthleteName $name,
        ?AthleteSport $sport,
        ?AthleteCountryCode $country
    ): array {
        $dqlAlias = 'a';
        $queryBuilder = $this->createQueryBuilder($dqlAlias);

        $specification = new AsArray(
            new AndX(
                new FilterId($id),
                new FilterAthleteName($name),
                new FilterAthleteSport($sport),
                new FilterAthleteCountry($country)
            )
        );

        $allValuesAreNull = empty(array_filter(
            func_get_args(),
            static function ($value) {
                return null !== $value;
            }
        ));

        if (false === $allValuesAreNull) {
            $queryBuilder->where($specification->match($queryBuilder, $dqlAlias));
        }

        $query = $queryBuilder->getQuery();

        $specification->hydrate($query);

        return $query->getResult();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function addAndSave(Athlete $athlete): void
    {
        $this->getEntityManager()->persist($athlete);
        $this->getEntityManager()->flush();
    }
    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(): void
    {
        $this->getEntityManager()->flush();
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function removeAndSave(Athlete $athlete): void
    {
        $this->getEntityManager()->remove($athlete);
        $this->getEntityManager()->flush();
    }


    public function beginTransaction(): void
    {
        $this->getEntityManager()->beginTransaction();
    }

    public function commit(): void
    {
        $this->getEntityManager()->commit();
    }
}
