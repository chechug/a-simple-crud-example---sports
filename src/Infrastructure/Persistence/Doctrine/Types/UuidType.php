<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;

final class UuidType extends Type
{
    public const UUID_TYPE = 'uuid_type';

    public function getName(): string
    {
        return self::UUID_TYPE;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf('CHAR(%d) COMMENT \'(DC2Type:uuid_type)\'', $fieldDeclaration['length']);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?Uuid
    {
        if (null !== $value) {
            $value = Uuid::from($value);
        }

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if ($value instanceof Uuid) {
            $value = $value->value();
        }

        return $value;
    }
}
