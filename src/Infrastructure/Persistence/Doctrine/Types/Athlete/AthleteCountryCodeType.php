<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Types\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class AthleteCountryCodeType extends Type
{
    public const ATHLETE_COUNTRY = 'athlete_country';

    public function getName(): string
    {
        return self::ATHLETE_COUNTRY;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf(
            'VARCHAR(%d) COMMENT \'(DC2Type:%s)\'',
            $fieldDeclaration['length'],
            $this->getName()
        );
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?AthleteCountryCode
    {
        if (null !== $value) {
            $value = AthleteCountryCode::from($value);
        }

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if ($value instanceof AthleteCountryCode) {
            $value = $value->value();
        }

        return $value;
    }
}
