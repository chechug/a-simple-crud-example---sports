<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Types\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class AthleteSportType extends Type
{
    public const ATHLETE_SPORT = 'athlete_sport';

    public function getName(): string
    {
        return self::ATHLETE_SPORT;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf(
            'ENUM(\'%s\', \'%s\', \'%s\', \'%s\') COMMENT \'(DC2Type:%s)\'',
            AthleteSport::FOOTBALL,
            AthleteSport::BASKET,
            AthleteSport::HOCKEY,
            AthleteSport::OTHER,
            $this->getName()
        );
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?AthleteSport
    {
        if (null !== $value) {
            $value = AthleteSport::from($value);
        }

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if ($value instanceof AthleteSport) {
            $value = $value->value();
        }

        return $value;
    }
}
