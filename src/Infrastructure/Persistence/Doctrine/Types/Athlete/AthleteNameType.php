<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Types\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;

final class AthleteNameType extends Type
{
    public const ATHLETE_NAME = 'athlete_name';

    public function getName(): string
    {
        return self::ATHLETE_NAME;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf(
            'VARCHAR(%d) COMMENT \'(DC2Type:%s)\'',
            $fieldDeclaration['length'],
            $this->getName()
        );
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?AthleteName
    {
        if (null !== $value) {
            $value = AthleteName::from($value);
        }

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform): string
    {
        if ($value instanceof AthleteName) {
            $value = $value->value();
        }

        return $value;
    }
}
