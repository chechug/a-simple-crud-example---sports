<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use ChechuProjects\Sports\Domain\Model\ValueObject\Enabled;

final class EnabledType extends Type
{
    public const ENABLED_TYPE = 'enabled';

    public function getName(): string
    {
        return self::ENABLED_TYPE;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf('TINYINT(%d) COMMENT \'(DC2Type:enabled)\'', $fieldDeclaration['length']);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return Enabled::from((bool) $value);
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof Enabled) {
            $value = (int) $value->value();
        }

        return $value;
    }
}
