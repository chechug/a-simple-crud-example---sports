<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Types\Type;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;

final class DateType extends Type
{
    public function getName(): string
    {
        return 'date_type';
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return sprintf('DATETIME(%d) COMMENT \'(DC2Type:date_type)\'', $fieldDeclaration['length']);
    }

    public function convertToPHPValue($value, AbstractPlatform $platform): ?DateTimeValueObject
    {
        if (null !== $value) {
            $value = DateTimeValueObject::from($value);
        }

        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ($value instanceof DateTimeValueObject) {
            $value = $value->format('Y-m-d');
        }

        return $value;
    }
}
