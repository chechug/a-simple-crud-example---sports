<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Application;

use League\Tactician\Middleware;
use ChechuProjects\Sports\Application\EventStore;
use ChechuProjects\Sports\Domain\DomainEventPublisher;

use ChechuProjects\Sports\Application\CollectInMemoryDomainEventSubscriber;
use ChechuProjects\Sports\Infrastructure\Messaging\Publisher\PublishAggregateMessage;

class DomainEventsMiddleware implements Middleware
{
    private $eventStore;
    private $publishMessage;

    public function __construct(
        EventStore $eventStore,
        PublishAggregateMessage $publishMessage
    ) {
        $this->eventStore = $eventStore;
        $this->publishMessage = $publishMessage;
    }

    public function execute($command, callable $next)
    {
        $domainEventPublisher = DomainEventPublisher::instance();
        $domainEventCollector = new CollectInMemoryDomainEventSubscriber();

        $domainEventPublisher->subscribe($domainEventCollector);

        $returnValue = $next($command);

        $events = $domainEventCollector->events();
        foreach ($events as $event) {
            $this->eventStore->append($event);
            $this->publishMessage->publish($event);
        }

        return $returnValue;
    }
}
