<?php

declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Migrations\Doctrine;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190622181959 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE athletes (id CHAR(36) COMMENT \'(DC2Type:uuid_type)\' NOT NULL COMMENT \'(DC2Type:uuid_type)\', name VARCHAR(255) COMMENT \'(DC2Type:athlete_name)\' NOT NULL COMMENT \'(DC2Type:athlete_name)\', sport ENUM(\'FOOTBALL\', \'BASKET\', \'HOCKEY\', \'OTHER\') COMMENT \'(DC2Type:athlete_sport)\' NOT NULL COMMENT \'(DC2Type:athlete_sport)\', birthday DATETIME(0) COMMENT \'(DC2Type:date_type)\' NOT NULL COMMENT \'(DC2Type:date_type)\', country VARCHAR(2) COMMENT \'(DC2Type:athlete_country)\' NOT NULL COMMENT \'(DC2Type:athlete_country)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event_store (id INT AUTO_INCREMENT NOT NULL, event_name VARCHAR(250) NOT NULL, event_body LONGTEXT NOT NULL, occurred_on DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE athletes');
        $this->addSql('DROP TABLE event_store');
    }
}
