<?php

declare(strict_types=1);

namespace ChechuProjects\Sports\Infrastructure\Fixtures\Doctrine\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AthleteFixture extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        $athlete = Athlete::create(
            Uuid::v4(),
            AthleteName::from('Andoni Iraola'),
            AthleteSport::from(AthleteSport::FOOTBALL),
            DateTimeValueObject::from('1977-05-05'),
            AthleteCountryCode::from('ES')
        );

        $manager->persist($athlete);
        $manager->flush();
    }
}
