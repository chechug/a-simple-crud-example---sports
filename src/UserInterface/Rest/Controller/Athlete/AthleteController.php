<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\UserInterface\Rest\Controller\Athlete;

use ChechuProjects\Sports\Application\Command\Athlete\Create\CreateAthleteCommand;
use ChechuProjects\Sports\Application\Command\Athlete\Delete\DeleteAthleteCommand;
use ChechuProjects\Sports\Application\Query\Athlete\FinderAthlete\FinderAthleteQuery;
use ChechuProjects\Sports\Domain\Model\Athlete\Exception\InvalidJSONException;
use League\Tactician\CommandBus;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

final class AthleteController extends AbstractController
{
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @throws InvalidJSONException
     */
    public function createAthlete(Request $request): JsonResponse
    {
        $decodedRequest = json_decode($request->getContent(), false);

        if (null === $decodedRequest) {
            throw new InvalidJSONException();
        }

        $command = CreateAthleteCommand::create(
            $decodedRequest->id,
            $decodedRequest->name,
            $decodedRequest->sport,
            $decodedRequest->birthday,
            $decodedRequest->country
        );

        $this->commandBus->handle($command);

        return new JsonResponse('', Response::HTTP_CREATED);
    }

    /**
     * @throws InvalidJSONException
     */
    public function finderAthlete(Request $request): JsonResponse
    {
        $decodedRequest = json_decode($request->getContent(), false);

        if (null === $decodedRequest) {
            throw new InvalidJSONException();
        }

        $query = FinderAthleteQuery::create(
            $request->query->get('id'),
            $request->query->get('name'),
            $request->query->get('sport'),
            $request->query->get('country')
        );

        return new JsonResponse($this->commandBus->handle($query), Response::HTTP_OK);
    }

    public function deleteAthlete(Request $request): JsonResponse
    {
        $command = DeleteAthleteCommand::create(
            $request->attributes->get('id')
        );

        $this->commandBus->handle($command);

        return new JsonResponse('', Response::HTTP_OK);
    }
}
