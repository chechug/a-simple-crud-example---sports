<?php

namespace ChechuProjects\Sports\UserInterface\Rest\Controller;

use ChechuProjects\Sports\Infrastructure\External\GeographicalService\Client\ApiGeographicalService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class MainController extends AbstractController
{
    public function index(): JsonResponse
    {
        return new JsonResponse('Welcome!', Response::HTTP_OK);
    }
}
