<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application;

use ChechuProjects\Sports\Domain\DomainEventSubscriber;
use ChechuProjects\Sports\Domain\Message\Message;

class CollectInMemoryDomainEventSubscriber implements DomainEventSubscriber
{
    private $events;

    public function __construct()
    {
        $this->events = [];
    }

    public function handle(Message $aDomainEvent): void
    {
        $this->events[] = $aDomainEvent;
    }

    public function isSubscribedTo(Message $aDomainEvent): bool
    {
        return true;
    }

    public function events(): array
    {
        return $this->events;
    }
}
