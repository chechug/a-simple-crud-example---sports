<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Query\Athlete\FinderAthlete;

use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;

final class FinderAthleteArrayDataTransformer implements FinderAthleteDataTransformerInterface
{
    /**
     * @param Athlete [] $athletes
     * @return array
     */
    public function transform(array $athletes): array
    {
        $athletesCollection = [];

        foreach ($athletes as $athlete) {
            $athletesCollection[] = [
                'id' => $athlete->id()->value(),
                'name' => $athlete->name()->value(),
                'birthday' => $athlete->birthday()->format('d-m-Y'),
                'sport' => $athlete->sport()->value(),
                'country' => $athlete->country()->value(),
            ];
        }

        return $athletesCollection;
    }
}
