<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Query\Athlete\FinderAthlete;

use Assert\Assert;

final class FinderAthleteQuery
{
    private $id;
    private $name;
    private $sport;
    private $country;

    public static function create($id, $name, $sport, $country): self
    {
        return new self($id, $name, $sport, $country);
    }

    public function id(): ?string
    {
        return $this->id;
    }

    public function name(): ?string
    {
        return $this->name;
    }

    public function sport(): ?string
    {
        return $this->sport;
    }

    public function country(): ?string
    {
        return $this->country;
    }

    private function __construct($id, $name, $sport, $country)
    {
        Assert::lazy()
            ->that($id, 'id')->nullOr()->uuid()
            ->that($name, 'name')->nullOr()->string()->notBlank()
            ->that($sport, 'sport')->nullOr()->string()->notBlank()
            ->that($country, 'country')->nullOr()->string()->notBlank()
            ->verifyNow();

        $this->id = $id;
        $this->name = $name;
        $this->sport = $sport;
        $this->country = $country;
    }
}
