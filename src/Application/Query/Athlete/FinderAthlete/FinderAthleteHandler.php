<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Query\Athlete\FinderAthlete;

use ChechuProjects\Sports\Domain\Model\Athlete\AthleteRepositoryInterface;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;

final class FinderAthleteHandler
{
    private $athleteRepository;
    private $dataTransform;

    public function __construct(
        AthleteRepositoryInterface $athleteRepository,
        FinderAthleteDataTransformerInterface $dataTransform
    ) {
        $this->athleteRepository = $athleteRepository;
        $this->dataTransform = $dataTransform;
    }

    public function handle(FinderAthleteQuery $query): array
    {
        $id = null !== $query->id() ? Uuid::from($query->id()) : null;
        $name = null !== $query->name() ? AthleteName::from($query->name()) : null;
        $sport = null !== $query->sport() ? AthleteSport::from(strtoupper($query->sport())) : null;
        $country = null !== $query->country() ? AthleteCountryCode::from($query->country()) : null;

        return $this->dataTransform->transform(
            $this->athleteRepository->filter($id, $name, $sport, $country)
        );
    }
}
