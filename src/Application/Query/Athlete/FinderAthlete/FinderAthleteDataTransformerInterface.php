<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Query\Athlete\FinderAthlete;

use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;

interface FinderAthleteDataTransformerInterface
{
    /**
     * @param Athlete [] $athletes
     * @return array
     */
    public function transform(array $athletes): array;
}
