<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Command\Athlete\Delete;

use Assert\Assert;

final class DeleteAthleteCommand
{
    private $id;

    public static function create($id): self
    {
        return new self($id);
    }

    public function id(): string
    {
        return $this->id;
    }
    private function __construct($id)
    {
        Assert::lazy()
            ->that($id, 'id')->uuid()
            ->verifyNow();

        $this->id = $id;
    }
}
