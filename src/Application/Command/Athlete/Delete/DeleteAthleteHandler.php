<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Command\Athlete\Delete;

use ChechuProjects\Sports\Domain\Model\Athlete\AthleteRepositoryInterface;
use ChechuProjects\Sports\Domain\Model\Athlete\Exception\AthleteNotFound;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;

final class DeleteAthleteHandler
{
    private $athleteRepository;

    public function __construct(AthleteRepositoryInterface $athleteRepository)
    {
        $this->athleteRepository = $athleteRepository;
    }

    /**
     * @throws AthleteNotFound
     */
    public function handle(DeleteAthleteCommand $command): void
    {
        $id = Uuid::from($command->id());
        $athlete = $this->athleteRepository->searchOne($id);

        if (null === $athlete) {
            throw AthleteNotFound::fromId($id);
        }

        $this->athleteRepository->removeAndSave($athlete);
    }
}
