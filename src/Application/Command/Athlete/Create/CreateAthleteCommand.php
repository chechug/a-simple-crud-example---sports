<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Command\Athlete\Create;

use Assert\Assert;

final class CreateAthleteCommand
{
    private $id;
    private $name;
    private $sport;
    private $birthday;
    private $country;

    public static function create($id, $name, $sport, $birthday, $country): self
    {
        return new self($id, $name, $sport, $birthday, $country);
    }

    public function id(): string
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function sport(): string
    {
        return $this->sport;
    }

    public function birthday(): string
    {
        return $this->birthday;
    }

    public function country(): string
    {
        return $this->country;
    }

    private function __construct($id, $name, $sport, $birthday, $country)
    {
        Assert::lazy()
            ->that($id, 'id')->uuid()
            ->that($name, 'name')->string()->notBlank()
            ->that($sport, 'sport')->string()->notBlank()
            ->that($birthday, 'birthday')->date('Y-m-d')
            ->that($country, 'country')->string()->notBlank()
            ->verifyNow();

        $this->id = $id;
        $this->name = $name;
        $this->sport = $sport;
        $this->birthday = $birthday;
        $this->country = $country;
    }
}
