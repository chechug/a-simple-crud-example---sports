<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application\Command\Athlete\Create;

use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\AthleteRepositoryInterface;
use ChechuProjects\Sports\Domain\Model\Athlete\Exception\AthleteAlreadyExists;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\DateTimeValueObject;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use Exception;

final class CreateAthleteHandler
{
    private $athleteRepository;

    public function __construct(AthleteRepositoryInterface $athleteRepository)
    {
        $this->athleteRepository = $athleteRepository;
    }

    /**
     * @throws Exception
     */
    public function handle(CreateAthleteCommand $command): void
    {
        $id = Uuid::from($command->id());

        $this->assertAthleteNotExists($id);

        $this->athleteRepository->addAndSave(
            Athlete::create(
                $id,
                AthleteName::from($command->name()),
                AthleteSport::from(strtoupper($command->sport())),
                DateTimeValueObject::from($command->birthday()),
                AthleteCountryCode::from($command->country())
            )
        );
    }

    /**
     * @throws AthleteAlreadyExists
     */
    private function assertAthleteNotExists(Uuid $id): void
    {
        $athlete = $this->athleteRepository->searchOne($id);

        if (null !== $athlete) {
            throw AthleteAlreadyExists::fromId($id);
        }
    }
}
