<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Application;

use ChechuProjects\Sports\Domain\Message\Types\DomainEvent;

interface EventStore
{
    public function append(DomainEvent $aDomainEvent);
    public function allStoredEventsSince($anEventId);
}
