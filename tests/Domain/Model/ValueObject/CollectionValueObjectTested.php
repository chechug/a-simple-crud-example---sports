<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Domain\Model\ValueObject;

use ChechuProjects\Sports\Domain\Model\ValueObject\CollectionValueObject;

class CollectionValueObjectTested extends CollectionValueObject
{
    public function add($item)
    {
        return $this->addItem($item);
    }

    public function remove($item)
    {
        return $this->removeItem($item);
    }
}
