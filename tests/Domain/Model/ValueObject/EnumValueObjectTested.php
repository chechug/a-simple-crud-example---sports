<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Domain\Model\ValueObject;

use ChechuProjects\Sports\Domain\Model\ValueObject\EnumValueObject;

class EnumValueObjectTested extends EnumValueObject
{
    private const ENUM_1 = '1';
    private const ENUM_2 = '2';
}
