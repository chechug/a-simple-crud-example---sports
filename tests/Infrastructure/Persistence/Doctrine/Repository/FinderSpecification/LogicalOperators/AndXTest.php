<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\LogicalOperators;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use Exception;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Generic\FilterId;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\FinderSpecification;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\LogicalOperators\AndX;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use ReflectionException;

final class AndXTest extends TestCase
{
    /**
     * @var MockObject
     */
    private $queryBuilder;

    /**
     * @throws ReflectionException
     * @throws Exception
     */
    protected function setUp(): void
    {
        $this->queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
        new Comparison('id', '=', Uuid::v4());
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_a_and_x_when_not_had_filters_then_andx_is_empty(): void
    {
        $andX = new AndX(new FilterId(null));

        $this->queryBuilder
            ->expects($this->atLeastOnce())
            ->method('expr')
            ->willReturn(new Expr());

        $this->assertInstanceOf(
            Expr\Andx::class,
            $andX->match($this->queryBuilder, 'alias')
        );

        $this->assertSame(
            [],
            $andX->match($this->queryBuilder, 'alias')->getParts()
        );
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_a_and_x_when_have_filters_then_and_x_have_comparison(): void
    {
        $alias = 'alias';
        $andX = new AndX($this->getFilterDeltaId());

        $comparison = new Comparison("{$alias}.deltaId", '=', ':deltaId');

        $this->queryBuilder
            ->expects($this->atLeastOnce())
            ->method('expr')
            ->willReturn(new Expr());

        $this->assertInstanceOf(
            Expr\Andx::class,
            $andX->match($this->queryBuilder, $alias)
        );

        $this->assertEquals(
            [$comparison],
            $andX->match($this->queryBuilder, $alias)->getParts()
        );
    }

    /**
     * @throws Exception
     */
    private function getFilterDeltaId(): FinderSpecification
    {
        return new class(Uuid::v4()) implements FinderSpecification {
            private $deltaId;

            public function __construct(?Uuid $deltaId)
            {
                $this->deltaId = $deltaId;
            }

            public function match(QueryBuilder $queryBuilder, string $alias): ?Comparison
            {
                if (null === $this->deltaId) {
                    return null;
                }

                $queryBuilder->setParameter('deltaId', $this->deltaId);
                return $queryBuilder->expr()->eq("{$alias}.deltaId", ':deltaId');
            }
        };
    }
}
