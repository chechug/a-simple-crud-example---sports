<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Generic;

use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use Exception;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Generic\FilterId;
use PHPUnit\Framework\TestCase;
use ReflectionException;

final class FilterIdTest extends TestCase
{
    private $queryBuilder;

    /**
     * @throws ReflectionException
     */
    protected function setUp(): void
    {
        $this->queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function given_a_filter_id_when_is_null_return_null(): void
    {
        $filter = new FilterId(null);

        $this->assertNull($filter->match($this->queryBuilder, 'alias'));
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_a_filter_id_when_it_contains_an_uuid_then_return_comparison(): void
    {
        $filter = new FilterId(Uuid::v4());

        $this->queryBuilder
            ->expects($this->once())
            ->method('expr')
            ->willReturn(new Expr());

        $this->assertInstanceOf(Comparison::class, $filter->match($this->queryBuilder, 'alias'));
    }
}
