<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteName;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete\FilterAthleteName;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use Exception;
use PHPUnit\Framework\TestCase;

final class FilterAthleteNameTest extends TestCase
{
    private $queryBuilder;

    protected function setUp(): void
    {
        $this->queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function given_a_filter_athlete_country_when_is_null_return_null(): void
    {
        $filter = new FilterAthleteName(null);

        $this->assertNull($filter->match($this->queryBuilder, 'alias'));
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_a_filter_id_when_it_contains_an_uuid_then_return_comparison(): void
    {
        $filter = new FilterAthleteName(AthleteName::from('Chechu'));

        $this->queryBuilder
            ->expects($this->once())
            ->method('expr')
            ->willReturn(new Expr());

        $this->assertInstanceOf(Comparison::class, $filter->match($this->queryBuilder, 'alias'));
    }
}
