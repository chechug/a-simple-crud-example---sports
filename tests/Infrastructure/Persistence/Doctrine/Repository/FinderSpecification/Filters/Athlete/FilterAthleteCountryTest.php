<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete;

use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteCountryCode;
use ChechuProjects\Sports\Infrastructure\Persistence\Doctrine\Repository\FinderSpecification\Filters\Athlete\FilterAthleteCountry;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Expr\Comparison;
use Doctrine\ORM\QueryBuilder;
use Exception;
use PHPUnit\Framework\TestCase;

final class FilterAthleteCountryTest extends TestCase
{
    private $queryBuilder;

    protected function setUp(): void
    {
        $this->queryBuilder = $this->getMockBuilder(QueryBuilder::class)
            ->disableOriginalConstructor()
            ->getMock();
    }

    /**
     * @test
     */
    public function given_a_filter_athlete_country_when_is_null_return_null(): void
    {
        $filter = new FilterAthleteCountry(null);

        $this->assertNull($filter->match($this->queryBuilder, 'alias'));
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_a_filter_id_when_it_contains_an_uuid_then_return_comparison(): void
    {
        $filter = new FilterAthleteCountry(AthleteCountryCode::from('ES'));

        $this->queryBuilder
            ->expects($this->once())
            ->method('expr')
            ->willReturn(new Expr());

        $this->assertInstanceOf(Comparison::class, $filter->match($this->queryBuilder, 'alias'));
    }
}
