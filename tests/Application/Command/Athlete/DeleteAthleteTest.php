<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Application\Command\Athlete;

use ChechuProjects\Sports\Application\Command\Athlete\Delete\DeleteAthleteCommand;
use ChechuProjects\Sports\Application\Command\Athlete\Delete\DeleteAthleteHandler;
use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\AthleteRepositoryInterface;
use ChechuProjects\Sports\Domain\Model\Athlete\Exception\AthleteNotFound;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use PHPUnit\Framework\TestCase;

final class DeleteAthleteTest extends TestCase
{
    private $athleteRepository;

    protected function setUp(): void
    {
        $this->athleteRepository = $this
            ->getMockBuilder(AthleteRepositoryInterface::class)
            ->getMock();
    }

    /**
     * @throws AthleteNotFound
     * @test
     */
    public function given_a_id_when_not_exists_in_db_then_throw_exception(): void
    {
        $id = Uuid::v4();
        
        $this->athleteRepository
            ->expects($this->once())
            ->method('searchOne')
            ->with($id)
            ->willReturn(null);

        $this->expectException(AthleteNotFound::class);

        (new DeleteAthleteHandler($this->athleteRepository))
            ->handle(DeleteAthleteCommand::create($id->value()));
    }

    /**
     * @test
     */
    public function given_a_id_when_exists_in_db_then_remove(): void
    {
        $id = Uuid::v4();

        $athleteStub = $this
            ->getMockBuilder(Athlete::class)
            ->disableOriginalConstructor()
            ->getMock();

        $this->athleteRepository
            ->expects($this->once())
            ->method('searchOne')
            ->with($id)
            ->willReturn($athleteStub);

        $this->athleteRepository
            ->expects($this->once())
            ->method('removeAndSave')
            ->with($athleteStub);

        (new DeleteAthleteHandler($this->athleteRepository))
            ->handle(DeleteAthleteCommand::create($id->value()));
    }
}
