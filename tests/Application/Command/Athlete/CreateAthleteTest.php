<?php
declare(strict_types=1);

namespace ChechuProjects\Sports\Tests\Application\Command\Athlete;

use ChechuProjects\Sports\Application\Command\Athlete\Create\CreateAthleteCommand;
use ChechuProjects\Sports\Application\Command\Athlete\Create\CreateAthleteHandler;
use ChechuProjects\Sports\Domain\Model\Athlete\Athlete;
use ChechuProjects\Sports\Domain\Model\Athlete\AthleteRepositoryInterface;
use ChechuProjects\Sports\Domain\Model\Athlete\Exception\AthleteAlreadyExists;
use ChechuProjects\Sports\Domain\Model\Athlete\ValueObject\AthleteSport;
use ChechuProjects\Sports\Domain\Model\ValueObject\Uuid;
use Exception;
use PHPUnit\Framework\TestCase;

final class CreateAthleteTest extends TestCase
{
    private $athleteRepository;

    protected function setUp(): void
    {
        $this->athleteRepository = $this
            ->getMockBuilder(AthleteRepositoryInterface::class)
            ->getMock();
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_create_new_athlete_when_id_already_exists_then_throw_exception(): void
    {
        $id = Uuid::v4();

        $athleteStub = $this
            ->getMockBuilder(Athlete::class)
            ->disableOriginalConstructor()
            ->getMock();

        $command = CreateAthleteCommand::create(
            $id->value(),
            'Chechu',
            AthleteSport::BASKET,
            '1990-10-10',
            'ES'
        );

        $this->athleteRepository
            ->expects($this->once())
            ->method('searchOne')
            ->with($id)
            ->willReturn($athleteStub);

        $this->expectException(AthleteAlreadyExists::class);

        (new CreateAthleteHandler($this->athleteRepository))->handle($command);
    }

    /**
     * @test
     * @throws Exception
     */
    public function given_create_new_athlete_when_info_is_ok_then_persist_in_db(): void
    {
        $id = Uuid::v4();

        $command = CreateAthleteCommand::create(
            $id->value(),
            'Chechu',
            AthleteSport::BASKET,
            '1990-10-10',
            'ES'
        );

        $this->athleteRepository
            ->expects($this->once())
            ->method('searchOne')
            ->with($id)
            ->willReturn(null);

        $this->athleteRepository
            ->expects($this->once())
            ->method('addAndSave');

        (new CreateAthleteHandler($this->athleteRepository))->handle($command);
    }
}
