Feature: Prueba de acceso a la api del example-Service
  Como un usuario anónimo
  Scenario: Probamos Headers y resultado
    When I send a GET request to "/example"
    And the header "Content-Type" should be equal to "application/json"
    Then the response status code should be 200